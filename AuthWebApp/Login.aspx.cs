﻿using System;
using System.IO;
using System.DirectoryServices;

namespace AuthWebApp
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            Boolean isValidUser = IsAuthenticated("dsslp", txtUser.Value.ToString().Trim(), txtPass.Value.ToString());
            if (isValidUser)
            {
                Session["UserCode"] = txtUser.Value.ToString().Trim();
                Response.Redirect("~/Contact.aspx");
            }
            else
            {
                lblError.Text = "Login failed!";
            }
        }

        public bool IsAuthenticated(string domain, string username, string pwd)
        {
            string domainAndUsername = domain + @"\" + username;
            DirectoryEntry entry = new DirectoryEntry("LDAP://dsslp.com", domainAndUsername, pwd);

            try
            {
                //Bind to the native AdsObject to force authentication.
                object obj = entry.NativeObject;

                DirectorySearcher search = new DirectorySearcher(entry);

                search.Filter = "(SAMAccountName=" + username + ")";
                search.PropertiesToLoad.Add("cn");
                SearchResult result = search.FindOne();

                if (result != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                //throw ex;
                return false;
            }
        }

    }
}