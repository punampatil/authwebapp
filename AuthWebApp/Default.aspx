﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AuthWebApp._Default" %>

<%@ Register Assembly="DevExpress.Web.ASPxSpreadsheet.v19.1, Version=19.1.9.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpreadsheet" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.ASPxRichEdit.v19.1, Version=19.1.9.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxRichEdit" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v19.1, Version=19.1.9.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row">
        <div class="col-md-4">
            <br />
            <h5><b>Control:</b> AspxUploadControl</h5>
            <dx:ASPxUploadControl ID="UploadCtrl" runat="server" UploadMode="Auto" Width="280px" OnFileUploadComplete="UploadCtrl_FileUploadComplete" ShowProgressPanel="True" ShowUploadButton="True" ShowAddRemoveButtons="False" FileSystemSettings-UploadFolder="D:\\UploadTesting" UploadStorage="FileSystem" AdvancedModeSettings-EnableFileList="True" AdvancedModeSettings-EnableMultiSelect="True">
                <AdvancedModeSettings EnableMultiSelect="True" EnableFileList="True"></AdvancedModeSettings>
                <FileSystemSettings UploadFolder="D:\\UploadTesting" GenerateUniqueFileNamePrefix="False"></FileSystemSettings>
                <Border BorderColor="#000066" BorderStyle="Solid" />
            </dx:ASPxUploadControl>
            <br />
            <%--<asp:Label ID="lblStatus" Visible="false" runat="server" Font-Italic="true" Font-Size="11px" Font-Bold="true" Text="File upload status"></asp:Label>--%>
            <br />
            <br />
            <h5><b>Control:</b> RichEdit - Document Viewer</h5>
            <dx:ASPxRichEdit ID="richEdit" Visible="false" runat="server" WorkDirectory="~\App_Data\WorkDirectory"></dx:ASPxRichEdit>
            <br />
            <br />
            <h5><b>Control:</b> Spreadsheet - Document Viewer</h5>
            <dx:ASPxSpreadsheet ID="spreadSheet" Visible="false" runat="server" WorkDirectory="~/App_Data/WorkDirectory"></dx:ASPxSpreadsheet>
            <br />
            <br />
            <h5><b>Control:</b> Image - Document Viewer</h5>

            <br />
            <br />
            <h5><b>Control:</b> ASPxFileManager</h5>
            <asp:Panel ID="Panel1" runat="server" Height="288px" Width="1000px">
                <dx:ASPxFileManager ID="FileMngr" Height="295px" Width="1000px" runat="server" OnSelectedFileOpened="FileMngr_SelectedFileOpened">
                    <SettingsFileList>
                        <DetailsViewSettings AllowColumnResize="True">
                        </DetailsViewSettings>
                    </SettingsFileList>
                    <SettingsEditing AllowDelete="True" AllowDownload="True" AllowRename="True" />
                    <SettingsUpload>
                        <AdvancedModeSettings EnableMultiSelect="True">
                        </AdvancedModeSettings>
                    </SettingsUpload>
                </dx:ASPxFileManager>
            </asp:Panel>
            <br />
            <br />
            <br />
            <%--<dx:ASPxImageGallery ID="ASPxImageGallery1" runat="server">
                <SettingsFolder ImageCacheFolder="~/Thumb/" />
            </dx:ASPxImageGallery>--%>
            <p>
                <asp:Button ID="btnContact" runat="server" Text="Go To Contact" OnClick="btnContact_Click" />
            </p>
        </div>
    </div>

</asp:Content>
