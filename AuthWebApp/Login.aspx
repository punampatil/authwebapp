﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="AuthWebApp.Login" %>

<%@ Register Assembly="DevExpress.Web.v19.1, Version=19.1.9.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<!DOCTYPE html>
<link rel="stylesheet" href="CSS/Login.css" type="text/css" media="screen" />
<script src="Scripts/WebForms/login.js" type="text/javascript"></script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        
    </style>
</head>
<body>
    <div class="login-container">
        <section class="login" id="login">
            <header>
                <h2>ENDEMOL SHINE INDIA</h2>
                <h4>Temporary Retainer Management System</h4>
            </header>
            <form id="form1" runat="server">
                <asp:RequiredFieldValidator ID="rfvTxtUser" ToolTip="Username required" runat="server" class="required-validator" ControlToValidate="txtUser" ErrorMessage="*"></asp:RequiredFieldValidator>
                <input type="text" runat="server" class="login-input" placeholder="Username" id="txtUser" />
                <asp:RequiredFieldValidator ID="rfvtxtPass" ToolTip="Password required" runat="server" class="required-validator" ControlToValidate="txtPass" ErrorMessage="*"></asp:RequiredFieldValidator>
                <input type="password" runat="server" class="login-input" placeholder="Password" id="txtPass" />
                <asp:Label ID="lblError" runat="server" class="required-validator" Text=""></asp:Label>
                <%--<dx:ASPxLabel ID="lblError1" class="required-validator" runat="server" Text=""></dx:ASPxLabel>--%>
                <br />
                &nbsp;
                <asp:LinkButton ID="lbReset" runat="server" CssClass="login-linkbutton" Font-Underline="false">RESET</asp:LinkButton>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                 <asp:LinkButton ID="lbForgotPass" runat="server" CssClass="login-linkbutton" Font-Underline="false">FORGOT PASSWORD</asp:LinkButton>
                <div class="submit-container">
                    <asp:Button ID="btnLogin" runat="server" CssClass="login-button" Text="LOGIN" OnClick="btnLogin_Click" />
                </div>
            </form>
        </section>
    </div>
</body>
</html>
