﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AuthWebApp
{
    public partial class Contact : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserCode"].ToString().Trim() == string.Empty)
            {
                Response.Redirect("~/Login.aspx");
            }
            else
            {
                lblStatus.Text = "Welcome " + Session["UserCode"].ToString().Trim() + " !!!";
            }
        }
    }
}