﻿using DevExpress.Web;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AuthWebApp
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Session["UserCode"].ToString().Trim() == string.Empty)
            //{
            //    Response.Redirect("~/Login.aspx");
            //}

            if (!IsPostBack)
            {
                Session["UserCode"] = "Punam";
                this.UploadCtrl.FileSystemSettings.UploadFolder = Application["UploadFolderPath"].ToString();

                Response.Write("Welcome dear " + Session["UserCode"].ToString());
                this.FileMngr.Settings.RootFolder = "D:\\UploadTesting";
            }
        }

        protected void btnContact_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Contact.aspx");
        }

        protected void UploadCtrl_FileUploadComplete(object sender, DevExpress.Web.FileUploadCompleteEventArgs e)
        {
            //lblStatus.Text = "Uploaded Files: " + e.UploadedFile.FileName.ToString();
        }

        protected void FileMngr_SelectedFileOpened(object source, DevExpress.Web.FileManagerFileOpenedEventArgs e)
        {
            FileManagerFile file = e.File;
            if (file.Extension.ToUpper() == ".DOC" || file.Extension.ToUpper() == ".DOCX")
            {
                richEdit.Visible = true;
                spreadSheet.Visible = false;
                richEdit.Open(file.FullName);
            }
            else if (file.Extension.ToUpper() == ".XLS" || file.Extension.ToUpper() == ".XLSX")
            {

                richEdit.Visible = false;
                spreadSheet.Visible = true;
                spreadSheet.Open(file.FullName);
            }
            else
            {
                richEdit.Visible = false;
                spreadSheet.Visible = false;

                System.Diagnostics.Process.Start(file.FullName);
            }

        }
    }
}